const express = require('express')
const api = express.Router()

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// GET t1
api.get('/t17', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t17/index.ejs',
        { title: 'Team1', layout: 'layout.ejs' })
})
api.get('/t17/c', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t17/c/index.ejs',
        { title: 'Blake Picklo', layout: 'layout.ejs' })
})
api.get('/t17/b', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t17/b/index.ejs',
        { title: 'Rakesh Varma Nadakudhiti', layout: 'layout.ejs' })
})
api.get('/t17/a', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t17/a/index.ejs',
        { title: 'SRAVYA KANCHARLA', layout: 'layout.ejs' })
})

module.exports = api
