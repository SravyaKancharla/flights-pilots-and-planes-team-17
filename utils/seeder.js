// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')
const puppies = require('../data/puppies.json')
const planes = require('../data/plane.json')
const pilots = require('../data/pilot.json')
const flights=require('../data/flight.json')

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = new Datastore()
  db.loadDatabase()


  // insert the sample data into our data store
  db.insert(puppies)
  db.insert(planes)
  db.insert(flights)
  db.insert(pilots)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.puppies = db.find(puppies)
  app.locals.planes = db.find(planes)
  app.locals.flights = db.find(flights)
  app.locals.pilots = db.find(pilots)

  
  LOG.debug(`${planes.length} plane`)
  LOG.debug(`${flights.length} flight`)
  LOG.debug(`${pilots.length} pilot`)
  
  LOG.info('END Seeder. Sample data read and verified.')
}